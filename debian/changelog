glpeces (5.2-1) unstable; urgency=low

   * New upstream version:
     - One new tangram game (hexagonal)
       created by I. De Marchi.
     - More 7,000 new shapes to play (31,610 in total).
     - Random generation of new shapes
       (not for all tangram game).
     - Improvement of the pieces rotation
       (Closes: #808409).
     - Add the glpeces.appdata.xml description file.
     - Migrated to Qt 5.
   * Changes in debian/control:
     - Update Standards-Version to 3.9.6.1: no special changes required.
     - Update depends and replaces fields.
     - Update Description fields.
     - Update Build-Depends to Qt 5 updated.
   * Update years in copyright file.
   * Add /usr/share/appdata dir in glpeces.dirs file.
     - As a result, update glpeces.install.
   * Remove debian/glpeces.menu file by the tech-ctte decision on #741573.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 08 Jan 2016 17:37:05 +0100

glpeces (5.1.1-1) unstable; urgency=low

   * New upstream version.
     - Removed all shadow backup files from sources.
   * Delete duplicate text license in debian/copyright file.
     
 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 15 May 2015 17:22:45 +0100

glpeces (5.1-1) experimental; urgency=low

   * New upstream release:
     - Add cmake build system.
     - For other changes, see changelog in upstream sources.
   * Changes in debian/control:
     - Update Standards-Version to 3.9.6: no special changes required.
     - Changes build-depends to cmake.
     - Add Vcs-Browser and Vcs-Git fields.
     - Update depends and replaces fields.
     - Correct bug description: thanks to Daniel Ejsing-Duun
       (LP: #1076133).
   * Update years in copyright file.
   * Add the hardering flags to debian/rules.
     - Add dpkg-dev in build depends field in debian/control.
   * Add debian/glpeces.dirs file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Mon, 02 Feb 2015 18:16:20 +0200

glpeces (5.0-2) unstable; urgency=low

  * Add Breaks field to debian/control file.
  * Add Debian package version to Replaces field
    in debian/control file (Closes: #666151).
  * Update debian/copyright file to v. 1.0.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Thu, 29 Mar 2012 09:32:15 +0200

glpeces (5.0-1) unstable; urgency=low

  * New upstream realease:
    - Improved rapid movement of the pieces
      (Closes: #634322).
    - Added 1 new tangram (39 in total).
    - Automatic generation of figures.
    - Added over 11,000 new figures.
    - Customization the pieces color.
    - Improved function comparison of figures.
    - Revised translation into French
      (thanks to Philippe Moutou).
  * Updated Standards-Version (without package changes).  
  * Updated description field in debian/control.
  * Add new package (glpeces-data) with the
    architecture independent files
    - Added news files: glpeces.install,
      glpeces-data.dirs, glpeces-data.install.
  * Updated year in debian/copyright file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sun, 11 Mar 2012 11:35:10 +0200

glpeces (4.1-1) unstable; urgency=low

  * New upstream realease:
    - Added 5 new tangram.
    - Added over 2700 new figures.
    - New stop function.
    - Resizable main window.
  * Updated description field in debian/control.
  * Updated year in debian/copyright file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Mon, 21 Mar 2011 15:45:12 +0200

glpeces (4.0.2-1) unstable; urgency=low

  * New upstream release.
  * Revision of German translation (Thanks to Erik Pfannenstein
    and Hendrik Knackstedt).
  * Revision of Italian translation (Thanks to Vincenzo Campanella).
  * New Brazilian translation (Thanks to Adriano Rafael Gomes).
  * New debian/rules with dh $@.
  * Other minor changes:
    - Add question to user for erase records.
    - Save the solution user.
    - Improved placement of pieces on the child level.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 24 Dec 2010 17:33:08 +0200

glpeces (4.0.1-1) unstable; urgency=low

  * New upstream release.
  * Improved the movement of the pieces (Closes: #585878).
  * Improved reviewing whether the solutions are correct (Closes: #586038).
  * Updating Standards-Version (without package changes).
  * Improvements in the search figures.
  * Fixed bug in game timekeeping.
  * Review of the translation to Portuguese (Thanks to Américo Monteiro).

 -- Innocent De Marchi <tangram.peces@gmail.com>  Mon, 18 Oct 2010 19:03:50 +0200

glpeces (4.0-1) unstable; urgency=low

  * Initial release (Closes: #502229)

 -- Innocent De Marchi <tangram.peces@gmail.com>  Thu, 20 May 2010 18:52:44 +0200

